#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
[SimConfig]
Sim_filename='Exp_01'
Sim_variables={'condition':[1,2],'pathology':[1,3]}
Sim_realizations={'Exp01':12}
Sim_name='E01'
Sim_hostname='jupiter'
[endSimConfig]
[SlurmConfig]
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar
#SBATCH --partition=internos
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --tasks-per-node=24
[endSlurmConfig]
"""

condition = 1
pathology = 1
Exp01 = 1

if Exp01 == 1:
    print(condition)
    print(pathology)
    print(Exp01)
