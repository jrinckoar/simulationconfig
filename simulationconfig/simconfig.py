#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simconfig
"""
import argparse
import collections
import itertools
import os
import string
import time

# Simulation variables, default values
Sim_filename = "file"
Sim_name = "Sim01"
Sim_variables = {}
Sim_realizations = {}
Sim_hostname = "cluster-fiuner"
Sim_MatExecutable = "matlab"
Sim_PyExecutable = "python3"

fname = ""

# Parse options
parser = argparse.ArgumentParser(
    description="publicar", formatter_class=argparse.RawTextHelpFormatter
)

parser.add_argument(
    "file", nargs=argparse.REMAINDER, metavar="file.py/.m", help="file python / matlab"
)
args = parser.parse_args()
fname = args.file[0]

# Parse option for matlab or python
ext = list(fname.split(".", 1))[1]
if ext == "py":
    comment_key = "#"
    endline = ""
    toLog = ""
else:
    comment_key = "%"
    endline = ";"
    toLog = (
        '\nlogfilename="{0}";\n'
        'fid=fopen(logfilename, "a");\n'
        'fprintf(fid, "%s: %s\\n", datestr(now, 0), "{1}");\n'
        "fclose(fid)\n"
    )


# Parse file inputs
class No_inFile(Exception):
    pass


# Read file options
def Read_File(initLine, endLine):
    """Read python or matlab file"""
    with open(fname) as file:
        content = file.readlines()
    content = [x.strip() for x in content]
    ind0 = content.index(initLine) + 1
    ind1 = content.index(endLine)
    content = content[ind0:ind1]
    content = [x for x in content if x != ""]
    return content


def OutFileName(keys, vals):
    """Create simulation files' names"""
    outname = Sim_filename
    opath = os.getcwd() + "/"
    simtag = ""
    for i in range(len(keys) - 1):
        s = keys[i]
        s = s[0].upper()
        v = vals[i]
        outname = outname + "_" + s + str(v)
        opath = opath + s + str(v) + "_"
        simtag = simtag + s + str(v) + ","
    if vals[-1] < 10:
        outname = outname + "_0" + str(vals[-1])
    else:
        outname = outname + "_" + str(vals[-1])
    o_name = opath[:-1] + "/" + outname + "." + ext
    return o_name, simtag[:-1], outname


# Write to simulation files
def WriteToFile(o_name, content, comment_key_):
    toWrite_ = content[:]

    text = ("{0} Generado automaticamente por SimConfig - {1}").format(
        comment_key_, time.strftime("%d/%m/%Y")
    )
    toWrite_.append(text)
    os.makedirs(os.path.dirname(o_name), exist_ok=True)
    with open(o_name, "w") as f:
        f.writelines(toWrite_)

    os.system("chmod +rwx " + o_name)


# Make simulation files
def Make_SimFiles():
    """Create simulation files"""
    # Copy file
    ouput_file_names = []
    simulationtags = []
    with open(fname) as file:
        content = file.readlines()
    content01 = [x.strip() for x in content]
    content02 = [x.replace(" ", "") for x in content01]
    # Variables combinations
    values = list(Sim_variables.values())
    rea = list(range(1, list(Sim_realizations.values())[0] + 1))
    values.append(rea)
    keys = list(Sim_variables.keys())
    keys.append(list(Sim_realizations.keys())[0])
    i = 0
    for vals in itertools.product(*values):
        to_Write = content[:]
        for j in range(len(keys)):
            search_exp = keys[j] + "=1" + endline
            replace_exp = keys[j] + " = " + str(vals[j]) + endline
            try:
                ind0 = content02.index(search_exp)
                to_Write[ind0] = replace_exp + "\n"
            except ValueError:
                print("Can not find {0} in input file".format(search_exp))

        o_name, simtag, outforlog = OutFileName(keys, vals)
        log_file = os.getcwd() + "/" + "log_" + Sim_name + ".txt"
        to_Write.append(toLog.format(log_file, outforlog))
        WriteToFile(o_name, to_Write, comment_key)
        ouput_file_names.append(o_name)
        simulationtags.append(simtag)
        i = i + 1
    return ouput_file_names, simulationtags


# Make slurm files
def Make_SLURM_Files(sim_files, sim_tags):
    alpha = list(string.ascii_lowercase)
    temp = [i + j for i, j in itertools.product(string.ascii_lowercase, repeat=2)]
    alpha = alpha + temp

    Slurm_file_names = []
    Slurmcontent = Read_File(initLine="[SlurmConfig]", endLine="[endSlurmConfig]")
    Slurmcontent.insert(0, "#!/bin/sh")
    rea = list(Sim_realizations.values())[0]
    k = 1
    for i in range(len(sim_files)):
        working_dir = os.path.dirname(sim_files[i])

        lineToWrite = Slurmcontent[:]

        text = (
            "#SBATCH --error={0}/job.%J.err\n" "#SBATCH --output={0}/job.%J.out"
        ).format(working_dir)

        lineToWrite.append(text)

        if k > rea:
            k = 1

        jobname_ = "".join([Sim_name] + sim_tags[i].split(",") + [alpha[k - 1]])

        lineToWrite.append("#SBATCH --job-name=" + jobname_)

        lineToWrite.append("cd " + working_dir)

        outfile_ = "out0" + str(k) + ".txt"
        errfile_ = "err0" + str(k) + ".txt"
        if k > 9:
            outfile_ = "out" + str(k) + ".txt"
            errfile_ = "err" + str(k) + ".txt"
        k = k + 1

        simfilename_ = os.path.basename(sim_files[i])
        if ext == "py":
            text = (
                "chmod 777 ./{0}\nexport PYTHONUNBUFFERED=1\n./{0} >{1} 2>{2}".format(
                    simfilename_, outfile_, errfile_
                )
            )
            lineToWrite.append(text)
        elif ext == "m":
            text = (
                "# execute script in matlab\n"
                "{3} -nodesktop -nodisplay -nosplash < {0} >{1} 2>{2}\n"
            ).format(simfilename_, outfile_, errfile_, Sim_MatExecutable)
            lineToWrite.append(text)

        lineToWrite = list(l + "\n" for l in lineToWrite)
        sbatch_name = working_dir + "/SBATCH/sbatch_" + jobname_ + ".sh"
        Slurm_file_names.append(sbatch_name)
        WriteToFile(sbatch_name, lineToWrite, "#")
    return Slurm_file_names


# Make launcher scripts files for cluster-uner
def Make_launchers_cluster(slurm_files, sim_tags):

    launcher_file_names = []
    jobname_array = []
    lineToWrite = ["#!/bin/sh"]

    # log file
    log_file = os.getcwd() + "/" + "log_" + Sim_name + ".txt"
    open(log_file, "w+").close()

    rea = list(Sim_realizations.values())[0] - 1
    k = 0
    for i in range(len(slurm_files)):

        temp = os.path.dirname(slurm_files[i]).split("/")
        working_dir = "/".join(temp[:-1]) + "/"
        jobname_ = "".join([Sim_name] + sim_tags[i].split(","))
        temp_ = os.path.basename(slurm_files[i])

        process_string = "{0}/{1}".format(k + 1, rea + 1)
        if k + 1 < 10:
            process_string = "0{0}/{1}".format(k + 1, rea + 1)

        text = (
            "#============================================================\n"
            "# Proceso {3} \n"
            'echo "lanzando proceso: {0} "\n'
            "sbatch {2} &\n"
            "sleep 0.3\n"
        ).format(temp_, log_file, slurm_files[i], process_string)

        lineToWrite.append(text)
        k = k + 1
        if k > rea:
            # lineToWrite.append('squeue -a')
            lineToWrite = list(l + "\n" for l in lineToWrite)
            jobname_ = "".join([Sim_name] + sim_tags[i].split(","))
            launcher_name = working_dir + "launcher" + jobname_ + ".sh"
            jobname_array.append(jobname_)
            launcher_file_names.append(launcher_name)
            WriteToFile(launcher_name, lineToWrite, "#")
            lineToWrite = ["#!/bin/sh"]
            k = 0
    # General launcher
    working_dir = os.getcwd() + "/"
    gen_launcher_name = working_dir + "run_" + Sim_name + ".sh"
    lineToWrite = ["#!/bin/sh"]
    for i, j in zip(
        jobname_array,
        launcher_file_names,
    ):
        text = (
            "#===========================================================\n"
            "# {0}\n"
            "{1}\n"
            "sleep 0.5"
        ).format(i, j)
        lineToWrite.append(text)
        # lineToWrite.append("sleep 0.5")
    lineToWrite.append(
        ("#===========================================================\n" "squeue -a")
    )

    lineToWrite = list(l + "\n" for l in lineToWrite)
    WriteToFile(gen_launcher_name, lineToWrite, "#")


# Make launcher scripts files for remote computer
def Make_launchers_remote(slurm_files, sim_tags):

    launcher_file_names = []
    lineToWrite = ["#!/bin/sh"]

    # log file
    log_file = os.getcwd() + "/" + "log_" + Sim_name + ".txt"
    open(log_file, "w+").close()

    rea = list(Sim_realizations.values())[0] - 1
    k = 0
    for i in range(len(slurm_files)):
        temp = os.path.dirname(slurm_files[i]).split("/")
        working_dir = "/".join(temp[:-1]) + "/"
        jobname_ = "".join([Sim_name] + sim_tags[i].split(","))
        log_file = os.getcwd() + "/" + "log_" + Sim_name + ".txt"
        temp_ = os.path.basename(slurm_files[i])
        text = (
            'echo "lanzando proceso: {0} " >> "{1}"\n'
            "sh {2} &\n"
            "lpidt=$!\n"
            'echo "PIDs:" >> "{1}"\n'
            'ps -ef | grep {3} >> "{1}"\n'
            "wait $lpidt\n"
            'echo "-------------------------------------------" >> "{1}"\n'
            "##==========================================================="
        ).format(temp_, log_file, slurm_files[i], Sim_name)

        lineToWrite.append(text)
        k = k + 1
        if k > rea:
            # lineToWrite.append('squeue -a')
            lineToWrite = list(l + "\n" for l in lineToWrite)
            jobname_ = "".join([Sim_name] + sim_tags[i].split(","))
            launcher_name = working_dir + "launcher" + jobname_ + ".sh"
            launcher_file_names.append(launcher_name)
            WriteToFile(launcher_name, lineToWrite, "#")
            lineToWrite = ["#!/bin/sh"]
            k = 0
    # General launcher
    working_dir = os.getcwd() + "/"
    gen_launcher_name = working_dir + "run_" + Sim_name + ".sh"
    lineToWrite = ["#!/bin/sh"]
    text = (
        'LOGFILE="{0}"\n'
        'echo "PID=$$" >> "$LOGFILE"\n'
        "##..............................................................."
    ).format(log_file)
    lineToWrite.append(text)
    for i in launcher_file_names:
        text = (
            'echo "{0}" >> "$LOGFILE"\n'
            "{0} &\n"
            "lpid=$!\n"
            "wait $lpid\n"
            'echo "-------------------"'
            ' >> "$LOGFILE"\n'
            "##==========================================================="
        ).format(i)
        lineToWrite.append(text)
    lineToWrite = list(l + "\n" for l in lineToWrite)
    WriteToFile(gen_launcher_name, lineToWrite, "#")


def main():
    global Sim_variables

    # Create simulation files
    Simcontent = Read_File(initLine="[SimConfig]", endLine="[endSimConfig]")
    [exec(x, globals()) for x in Simcontent]
    Sim_variables = collections.OrderedDict(sorted(Sim_variables.items()))
    output_file_names, simulationtags = Make_SimFiles()
    # Create slurm files
    slurm_file_names = Make_SLURM_Files(output_file_names, simulationtags)
    # Create launchers
    if Sim_hostname == "cluster-fiuner":
        Make_launchers_cluster(slurm_file_names, simulationtags)
    elif Sim_hostname == "jupiter":
        Make_launchers_cluster(slurm_file_names, simulationtags)
    else:
        Make_launchers_remote(slurm_file_names, simulationtags)

    print(
        "\n".join(
            [
                "SimConfig.",
                "'Juan Felipe Restrepo <juan.restrepo@uner.edu.ar>' 2022",
            ]
        )
    )


if __name__ == "__main__":
    main()
