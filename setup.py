from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='simulationconfig',
      version='2.0',
      description='Configuración de Simulaciones',
      long_description=readme(),
      url='https://jrinckoar@bitbucket.org/jrinckoar/simulationconfig.git',
      author='Juan F. Restrepo',
      author_email='jrestrepo@ingenieria.uner.edua.ar',
      license='MIT',
      packages=['simulationconfig'],
      entry_points={
          'console_scripts': ['simconfig = simulationconfig.simconfig:main'],
      },
      install_requires=[
          'progress',
      ],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      zip_safe=False)
