--------------------------------------------------------------------------------------------------------------------
# Simulation Config -- PYTHON CODE
## Juan F. Restrepo
### jrestrepo@ingenieria.uner.edu.ar

*Laboratorio de Señales y Dinámicas no Lineales,  Instituto de Bioingeniería y Bioinformática, CONICET - Universidad
Nacional de Entre Ríos.  Ruta prov 11 km 10 Oro Verde, Entre Ríos, Argentina.*

--------------------------------------------------------------------------------------------------------------------

### Instrucciones 

Uso: ./simconfig archivo.py/archivo.m


Dentro del archivo principal .m o .py debe haber una sección en forma de comentarios, ejemplo:

```
''' (para matlab usar %{) 
[SimConfig]
Sim_filename='Exp_01'                                 -> Nombre del archivo de la simulación. 
Sim_variables={'condition':[1,2],'pathology':[1,3]}   -> Nombre de las variables y sus valores. 
Sim_realizations={'Exp01':2}                          -> Nombre de la variable de realizaciones y número 
                                                         de realizaciones. 
Sim_hostname='cluster_uner'                           -> Nombre del Host. 
Sim_out_filename='out'                                -> Prefijo del archivo de salida. 
Sim_eout_filename='err'                               -> Prefijo del archivo de error. 
Sim_name='E01'                                        -> Nombre del trabajo. 
[endSimConfig] 
[SlurmConfig] 
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar 
#SBATCH --partition=internos 
#SBATCH --nodes=1 
#SBATCH --ntasks=24 
#SBATCH --tasks-per-node=24 
[endSlurmConfig] 
''' (para matlab usar %}) 
```

Ver example_matlab.m o example_python.py.

### Archivos
1. simulationgonfig.py
2. example_matlab.m
3. example_python.py

--------------------------------------------------------------------------------------------------------------------
Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>
