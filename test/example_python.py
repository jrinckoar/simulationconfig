#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
[SimConfig]
Sim_filename='Exp_01'
Sim_variables={'condition':[1,2],'pathology':[1,3]}
Sim_realizations={'Exp01':20}
Sim_name='E01'
Sim_hostname='jupiter'
[endSimConfig]
[SlurmConfig]
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar
#SBATCH --partition=debug
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --tasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:0
#SBATCH --mem=8G
[endSlurmConfig]
'''

condition = 1
pathology = 1
Exp01 = 1

if Exp01 == 1:
    print(condition)
    print(pathology)
    print(Exp01)
